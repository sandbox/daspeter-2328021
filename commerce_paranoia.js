/**
 * Adjust payment form handling to exclude certain fields from being sent back
 * to the server. This way we don't have to care about sensitive data somewhere
 * in the backend.
 */
;(function($) {
  /**
   * Enable all previously disabled payment detail elements.
   */
  Drupal.behaviors.commerceParanoiaPaymentDataSanitation = {
    attach: function (context, settings) {
      // Remove the disabled attribute on all form elements. If we can do this
      // it's very likely we also can adjust and thus sanitize the ajax request.
      $(':input[name^="commerce_payment[payment_details]"]', context).attr('disabled', null);
    }
  }

  // Backup Drupal core beforeSend function to extend it.
  var beforeSend = Drupal.ajax.prototype.beforeSend;
  // Add a trigger when beforeSend fires.
  Drupal.ajax.prototype.beforeSend = function(xmlhttprequest, options) {
    // Remove sensitive data.
    if (options.data) {
      // Convert data if not  a string.
      if(typeof(options.data) !== 'string') {
        var sanitized_data = {};
        $(options.data).each(function(i, elem) {
          if (elem.name.search(/^commerce_payment\[payment_details\]/) === -1) {
            sanitized_data[i] = elem;
          }
        });
        options.data = sanitized_data;
      }
      else {
        // Adjust data string.
        options.data = options.data.replace(/commerce_payment(%5B|\[)payment_details(%5D|\]).+?(&|&amp;|$)/gi, '');
      }
    }
    return beforeSend.call(this, xmlhttprequest, options);
  }
})(jQuery);
